# Wallet

Wallet is simple console application for managing virtual wallet.

Assumptions
-----------
There was no opportunity to clarify requirements so multiple assumptions had to be made.
These assumptions have drove the implementation as there was a "good will" to find a balance between the extensibility 
on the one hand and the simplicity on the other.

* The goal was to keep record of payments which may fit multiple domains (e.g. virtual wallet, bank account, stock exchange...),
for the simplicity **a virtual wallet** domain was chosen. This decision affects not only the naming of classes but also the 
structure of whole application.

* It was clear from requirements that application will require at least two threads (one for accepting console input and for
continuous printing balance), that raised question how the synchronization will be made. There were multiple options:
  * Purely event-based system where every action gets into the synchronized event queue which will be than
  handled by some thread(s) that will take care of execution (e.g. similarly like AWS event queue or JavaScript execution). 
  This solution would bring very good extensibility but for the bigger cost of simplicity so it was not used for now (but the 
  abstractions in the application themselves allow such implementation in future).
  * Purely immutable objects that would lead to eventual consistency when printing balance. Kind of a good option where any change in the balance
  would be reflected as a new Wallet instance. This solution would be nice from technical standpoint but not so much from "business"
  one as it is not usual to tell (print) the balance while in the same time it is being changed. Also the application is related directly
  with the money and thus consistency of data is preferred over availability.
  * Keyword synchronized for wallet operations. That seems as the sufficient option with easy implementation and likely
  "good enough" performance. However it is possible that wallet will be read by multiple clients at the same time which
  may reduce the overall performance significantly.
  * Read-write lock was used in the end. It is easy for the implementation but also allows multiple clients to read the balance
  as long as there is no other client currently changing the balance (adding od subtracting money). It also ensures consistency.
  
* The core of the application are money amounts and currencies. It would not make sense to implement the whole logic
of currencies from the beginning. There are many good external libraries but there is also Java specification 
[JSR-354](https://jcp.org/en/jsr/detail?id=354) that may make it to upcoming release of Java.
From this reason it was used in core parts of the system and the external dependency may be removed in the future.

Configuration
-------------
Wallet accepts typical command line arguments. To get the full explanation how to use the tool, use `-h` or `--help`.

These are the most common examples of arguments:
* ```-h``` = get help how to use the app
* `-w "./samples/wallet.txt"` = runs the wallet starting with initial sample commands from given file
* `-e "./samples/rates.txt"` = runs and uses exchange rates from given file (balance output then contains conversion to USD)
* `-w "./samples/wallet.txt" -e "./samples/rates.txt"` = both of above applies (full functionality)

Running
-------
As the application has interactive console, it is not advised to use gradle wrapper run task directly.
Instead it is recommended to build distributable package as follows:
```
./gradlew distZip
./gradlew distTar
```
** For Windows, use `.\gradlew.bat`

The archive is built into `build/distributions` directory and contains generated shell and cmd scripts for running the 
application (in the `bin` directory). To run the tool with these scripts, pass arguments as described above, e.g.:
```
bin/wallet -w "./samples/wallet.txt" -e "./samples/rates.txt"
```
** For Windows, use `wallet.bat`

Input Formats
-------------
The application runs until quit command comes from the user. If any invalid input is read, the message is presented to the client
but the application keeps running as there is no reason to halt the whole application.

At the moment, there are only two sources of input which are standard input and files which all use the same format.
Wallet commands as well as exchange rates (more about them in Implementation section) use both format of:

`<CURRENCY> <AMOUNT>`, e.g.: `USD 10`, `EUR -3.3`

The only restriction is that exchange rates must be values greater than zero. That is because exchange rate represent
a multiplicand used to get value in main currency (currently USD) - e.g. `GBP 1.3` means that 1 GBP = 1.3 USD. 
Zero or negative values simply does not make sense in this context.

Implementation
--------------
Classes and interfaces are described more or less extensively in its javadoc. However it worth to note few basic
principles to give a bigger picture:

* The core of whole application is `Wallet` interface that allows manipulation with the data. At the moment, there is
one main implementation `MemoryWallet` that holds the data in memory and one decorator `SynchronizedWallet` that ensures
thread safety of underlying wallet.

* The application run is driven by so-called `WalletCommand` instances that does the logic:
  * At the moment there are only two commands for changing balance and printing balance
  * Commands are generated by `WalletCommandProducer` instances that have one or more `WalletCommandListener` bound
  to them which decide what to do with the commands.
  * At the moment there is only one listener called `ImmediateExecutionListener` which executes command immediately from
  the caller thread but listeners generally leave room for future extensibility (e.g. executing commands in batched-like manner etc.)
  * There are 3 `WalletCommandProducer` implementations (reading from standard system input, reading from file,
  scheduled generation of the same command) but in future there may a lot more producers (e.g. commands may
  be read from network socket, other IO...).
    
* Exchange rates are simplified in this version by single multiplicand that is applied per currency to given money amount. 
They are read from the external file but if there would be more functional requirements, it would be advised to use more
complex logic of `javax.money.convert.ExchangeRate`.

* The parsing of input (both commands and exchange rates) is separated into parsers so they may be re-used for any kind of 
source (file, network, system in...). Right now there is not interface above them (as there is no indication of any
other implementation) but interface can be later easily introduced.

* The formatting of the output is made via `MonetaryAmountFormatter` that is part of `WalletFormatter`. The additional
conversion using exchange rates is made via decorator `AmountWithConversionFormatter`.
package online.zdenda.wallet;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import java.util.Collection;

/**
 * Wallet represents storage for {@link MonetaryAmount} that are stored per its {@link CurrencyUnit}.
 * <p>
 * It can be imagined as real life wallet with coins where also all amounts are also separated by its currency.
 * If the amount of any currency reaches zero after {@link #subtract(MonetaryAmount)}, the value is removed.
 * The difference between real life wallet is that this wallet can go to minus (negative values) per currency.
 */
public interface Wallet {

    /**
     * Gets current balance of the wallet where each returned {@link MonetaryAmount} represents amount per its
     * {@link CurrencyUnit}.
     *
     * @return balance of all currencies
     */
    Collection<MonetaryAmount> getBalance();

    /**
     * Adds given {@link MonetaryAmount} to the wallet under its currency.
     *
     * @param amount amount to be added
     */
    void add(MonetaryAmount amount);

    /**
     * Subtracts given {@link MonetaryAmount} from the wallet from its currency.
     * If the remaining value is zero, the entry is removed (and will not be returned via {@link #getBalance()}).
     *
     * @param amount amount to be subtracted
     */
    void subtract(MonetaryAmount amount);

}

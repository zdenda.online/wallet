package online.zdenda.wallet.format;

import javax.money.MonetaryAmount;

/**
 * Represents formatting of {@link MonetaryAmount} into {@link String} instance.
 * Note that we may have used {@link javax.money.format.MonetaryAmountFormat} but that does not satisfy all the
 * needs we have for formatting (e.g. printing converted values to specific currency).
 */
public interface MonetaryAmountFormatter {

    /**
     * Formats {@link MonetaryAmount} nto {@link String} instance.
     *
     * @param amount amount to be formatted
     * @return formatted amount
     */
    String format(MonetaryAmount amount);
}

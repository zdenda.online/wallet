package online.zdenda.wallet.format;

import javax.money.MonetaryAmount;
import java.text.NumberFormat;

/**
 * Represents simple formatting of {@link MonetaryAmount} as String of "CURRENCY_CODE VALUE" (e.g. "USD 10").
 */
public class SimpleAmountFormatter implements MonetaryAmountFormatter {

    @Override
    public String format(MonetaryAmount amount) {
        String formattedNumber = NumberFormat.getNumberInstance().format(amount.getNumber());
        return amount.getCurrency() + " " + formattedNumber;
    }
}

package online.zdenda.wallet.format;


import online.zdenda.wallet.Wallet;

import java.util.stream.Collectors;

/**
 * Represents simple formatting of {@link Wallet} where each amount is on its separate line formatted by given
 * {@link MonetaryAmountFormatter}.
 */
public class SimpleWalletFormatter implements WalletFormatter {

    public static final String PREFIX = "======= BALANCE =======\n";
    public static final String SUFFIX = "\n=======================";
    private final MonetaryAmountFormatter amountFormatter;

    public SimpleWalletFormatter(MonetaryAmountFormatter amountFormatter) {
        this.amountFormatter = amountFormatter;
    }

    @Override
    public String format(Wallet wallet) {
        String balance = wallet.getBalance().stream()
                .map(amountFormatter::format)
                .collect(Collectors.joining("\n"));
        return PREFIX + balance + SUFFIX;
    }
}

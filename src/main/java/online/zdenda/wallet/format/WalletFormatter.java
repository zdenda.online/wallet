package online.zdenda.wallet.format;

import online.zdenda.wallet.Wallet;

/**
 * Represents a formatting of {@link Wallet} (usually its balance) into {@link String}.
 */
public interface WalletFormatter {

    /**
     * Formats {@link Wallet} nto {@link String} instance.
     *
     * @param wallet wallet to be formatted
     * @return formatted wallet
     */
    String format(Wallet wallet);
}

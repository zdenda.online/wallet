package online.zdenda.wallet.format;

import online.zdenda.wallet.rate.ExchangeRate;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Represents a formatter that decorates any existing {@link MonetaryAmountFormatter} with additional information
 * about the value in given {@link CurrencyUnit} using custom exchange rage (multiplicand).
 * The additional value (number with currency) is formatted via decorated {@link MonetaryAmountFormatter}.
 * </p><p>
 * E.g. if you pass EUR for conversion of USD values and use {@link SimpleAmountFormatter} (formats to "USD 10"),
 * then the output may be something like "USD 10 (9 EUR)" = additional information is in parentheses.
 * </p>
 */
public class AmountWithConversionFormatter implements MonetaryAmountFormatter {

    private final MonetaryAmountFormatter decorated;
    private final CurrencyUnit mainCurrency;
    private final Map<CurrencyUnit, BigDecimal> currencyToMultiplicand;

    public AmountWithConversionFormatter(MonetaryAmountFormatter decorated,
                                         CurrencyUnit mainCurrency,
                                         Set<ExchangeRate> exchangeRates) {
        this.decorated = decorated;
        this.mainCurrency = mainCurrency;
        this.currencyToMultiplicand = convertExchangeRatesToMap(exchangeRates);
    }

    @Override
    public String format(MonetaryAmount amount) {
        String originalOutput = decorated.format(amount);
        BigDecimal multiplicand = currencyToMultiplicand.get(amount.getCurrency());

        if (multiplicand == null) return originalOutput;

        MonetaryAmount converted = Money.of(amount.getNumber(), mainCurrency).multiply(multiplicand);
        String additionalOutput = decorated.format(converted);
        return originalOutput + " (" + additionalOutput + ")";
    }

    /**
     * Convert to map for better performing look-up during formatting.
     */
    private Map<CurrencyUnit, BigDecimal> convertExchangeRatesToMap(Collection<ExchangeRate> exchangeRates) {
        Map<CurrencyUnit, BigDecimal> map = new HashMap<>();
        exchangeRates.forEach(r -> map.put(r.getCurrency(), r.getMultiplicand()));
        return map;
    }
}

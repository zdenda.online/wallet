package online.zdenda.wallet;

import online.zdenda.wallet.cmd.SystemOutBalanceCommand;
import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.ImmediateExecutionListener;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;
import online.zdenda.wallet.cmd.parser.WalletCommandParser;
import online.zdenda.wallet.cmd.producer.FileCommandsProducer;
import online.zdenda.wallet.cmd.producer.ScheduledCommandsProducer;
import online.zdenda.wallet.cmd.producer.SystemInCommandsProducer;
import online.zdenda.wallet.cmd.producer.WalletCommandsProducer;
import online.zdenda.wallet.format.*;
import online.zdenda.wallet.rate.ExchangeRate;
import online.zdenda.wallet.rate.parser.ExchangeRateParser;
import org.apache.commons.cli.*;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

/**
 * Main runnable class that parses command line arguments and initiates the application.
 * Run it with -h or --help option to see the usage.
 */
public class WalletMain {

    private static final CurrencyUnit MAIN_CURRENCY = Monetary.getCurrency("USD");

    public static void main(String[] args) {
        LogManager.getLogManager().reset(); // remove implicit loggers
        Options options = prepareCmdOptions();
        CommandLine cmdLine = parseArgs(options, args);

        printHelpIfWanted(cmdLine, options);

        Wallet wallet = new MemoryWallet();
        WalletCommandListener immediateExecutionListener = new ImmediateExecutionListener(wallet);
        WalletCommandParser cmdParser = new WalletCommandParser();
        List<WalletCommandsProducer> producers = new ArrayList<>(); // add producers that you want to invoke

        if (cmdLine.hasOption("w")) {
            File walletCommandsFile = toFile(cmdLine, "w");
            WalletCommandsProducer fileProducer = new FileCommandsProducer(walletCommandsFile, cmdParser);
            fileProducer.addListener(immediateExecutionListener);
            producers.add(fileProducer);
        }

        MonetaryAmountFormatter amountFormatter = new SimpleAmountFormatter();
        if (cmdLine.hasOption("e")) {
            File exchangeRatesFile = toFile(cmdLine, "e");
            ExchangeRateParser rateParser = new ExchangeRateParser();
            Set<ExchangeRate> exchangeRates = readExchangeRates(rateParser, exchangeRatesFile);
            amountFormatter = new AmountWithConversionFormatter(amountFormatter, MAIN_CURRENCY, exchangeRates);
        }
        WalletFormatter walletFormatter = new SimpleWalletFormatter(amountFormatter);
        WalletCommand sysOutWalletCommand = new SystemOutBalanceCommand(walletFormatter);
        ScheduledCommandsProducer printBalanceProducer = new ScheduledCommandsProducer(sysOutWalletCommand, 1, TimeUnit.MINUTES);
        printBalanceProducer.addListener(immediateExecutionListener);
        producers.add(printBalanceProducer);

        WalletCommandsProducer changeBalanceProducer = new SystemInCommandsProducer(cmdParser, printBalanceProducer);
        changeBalanceProducer.addListener(immediateExecutionListener);
        producers.add(changeBalanceProducer);

        producers.forEach(WalletCommandsProducer::start);
    }

    private static void printHelpIfWanted(CommandLine cmdLine, Options options) {
        if (cmdLine.hasOption("h")) {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.setWidth(80);
            helpFormatter.printHelp("wallet",
                    "Virtual wallet application that may be used to manage balance of your money\n\n",
                    options,
                    "\nSource code at: https://gitlab.com/zdenda.online/wallet",
                    true);
            System.exit(0);
        }
    }

    private static Options prepareCmdOptions() {
        return new Options()
                .addOption(Option.builder("w")
                        .longOpt("wallet-cmd-file")
                        .hasArg()
                        .argName("file")
                        .desc("Path to file with wallet commands to be applied at the start of application")
                        .build())
                .addOption(Option.builder("e")
                        .longOpt("exchange-rates-file")
                        .hasArg()
                        .argName("file")
                        .desc("Path to file with exchange rates against " + MAIN_CURRENCY)
                        .build())
                .addOption(Option.builder("h")
                        .longOpt("help")
                        .desc("Prints this message")
                        .build());
    }

    private static CommandLine parseArgs(Options options, String args[]) {
        try {
            return new DefaultParser().parse(options, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            System.exit(1);
            return null; // make compiler happy
        }
    }

    private static File toFile(CommandLine cmdLine, String opt) {
        String path = cmdLine.getOptionValue(opt);
        if (path == null || path.isEmpty()) {
            System.err.println("File path cannot be empty.");
            System.exit(2);
        }

        path = path.trim();
        File file = new File(path);

        if (!file.exists()) {
            System.err.println("File '" + path + "' does not exist.");
            System.exit(2);
        }
        if (!file.canRead()) {
            System.err.println("File '" + path + "' cannot be read by this process.");
            System.exit(2);
        }

        return file;
    }

    private static Set<ExchangeRate> readExchangeRates(ExchangeRateParser parser, File file) {
        try {
            Set<ExchangeRate> exchangeRates = new HashSet<>();
            Files.readAllLines(file.toPath(), StandardCharsets.UTF_8).forEach(line -> {
                Optional<ExchangeRate> rate = parser.parse(line);
                if (rate.isPresent()) {
                    exchangeRates.add(rate.get());
                } else {
                    System.err.println("'" + line + "' is not a valid exchange rate, this rate will not be used");
                }
            });
            return exchangeRates;
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(2);
            return null;
        }
    }
}

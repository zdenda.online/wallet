package online.zdenda.wallet.cmd;

import online.zdenda.wallet.Wallet;
import online.zdenda.wallet.cmd.producer.WalletCommandsProducer;

/**
 * Represents an instruction from external source (user, network stream, file...) that should be executed with {@link Wallet}.
 * Commands are usually created via {@link WalletCommandsProducer} implementations.
 *
 * @see WalletCommandsProducer
 */
public interface WalletCommand {

    /**
     * Executes the command on given {@link Wallet}.
     *
     * @param wallet wallet to be used for execution
     */
    void execute(Wallet wallet);
}

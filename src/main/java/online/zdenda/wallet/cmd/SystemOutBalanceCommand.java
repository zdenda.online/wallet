package online.zdenda.wallet.cmd;

import online.zdenda.wallet.Wallet;
import online.zdenda.wallet.format.WalletFormatter;

/**
 * Represents a command that prints current {@link Wallet} balance to the system output using given
 * {@link WalletFormatter}.
 *
 * @see WalletCommand
 */
public class SystemOutBalanceCommand implements WalletCommand {

    private final WalletFormatter formatter;

    public SystemOutBalanceCommand(WalletFormatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void execute(Wallet wallet) {
        String output = formatter.format(wallet);
        System.out.println(output);
    }
}

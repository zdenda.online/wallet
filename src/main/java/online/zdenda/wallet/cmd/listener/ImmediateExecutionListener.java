package online.zdenda.wallet.cmd.listener;

import online.zdenda.wallet.Wallet;
import online.zdenda.wallet.cmd.WalletCommand;

/**
 * Represents a listener that executes given {@link WalletCommand} immediately from the caller thread.
 */
public class ImmediateExecutionListener implements WalletCommandListener {

    private final Wallet wallet;

    public ImmediateExecutionListener(Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public void onCommand(WalletCommand command) {
        command.execute(wallet);
    }
}

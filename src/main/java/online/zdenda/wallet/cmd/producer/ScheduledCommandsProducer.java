package online.zdenda.wallet.cmd.producer;

import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;

import java.io.Closeable;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Represents a producer that produces given {@link WalletCommand} periodically at given rate.
 * It uses {@link ScheduledExecutorService} at the background, which means you must call {@link #close()}
 * to free resources of this service (mainly its threads so the application may correctly stop).
 */
public class ScheduledCommandsProducer implements WalletCommandsProducer, Closeable {

    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
    private final Collection<WalletCommandListener> listeners = new HashSet<>();

    private final WalletCommand command;
    private final long period;
    private final TimeUnit timeUnit;

    public ScheduledCommandsProducer(WalletCommand command, long period, TimeUnit timeUnit) {
        this.command = command;
        this.period = period;
        this.timeUnit = timeUnit;
    }

    @Override
    public void addListener(WalletCommandListener listener) {
        listeners.add(listener);
    }

    @Override
    public void start() {
        executorService.scheduleAtFixedRate(
                () -> listeners.forEach(listener -> listener.onCommand(command)), 0, period, timeUnit);
    }

    @Override
    public void close() {
        executorService.shutdownNow();
    }
}

package online.zdenda.wallet.cmd.producer;

import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;
import online.zdenda.wallet.cmd.parser.WalletCommandParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

/**
 * Represents a producer that loads commands from given {@link File} and notifies registered {@link WalletCommandListener} once.
 */
public class FileCommandsProducer implements WalletCommandsProducer {

    private final Collection<WalletCommandListener> listeners = new HashSet<>();

    private final File file;
    private final WalletCommandParser parser;

    public FileCommandsProducer(File file, WalletCommandParser parser) {
        this.file = file;
        this.parser = parser;
    }

    @Override
    public void addListener(WalletCommandListener listener) {
        listeners.add(listener);
    }

    @Override
    public void start() {
        if (!file.exists()) {
            System.err.println("Given file '" + file.getAbsolutePath() + "' does not exist. Nothing will be added");
            return;
        }

        try {
            Files.readAllLines(file.toPath(), StandardCharsets.UTF_8).forEach(line -> {
                Optional<WalletCommand> command = parser.parse(line);
                if (command.isPresent()) {
                    listeners.forEach(listener -> listener.onCommand(command.get()));
                } else {
                    System.err.println("'" + line + "' is not a valid command, no action performed for this line of file " + file.getAbsolutePath());
                }
            });
        } catch (IOException e) {
            System.err.println("Given file " + file.getAbsolutePath() + " cannot be read, its contents will be ignored");
        }
    }
}

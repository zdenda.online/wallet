package online.zdenda.wallet.cmd.producer;

import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;

/**
 * <p>
 * Represents a producer of {@link WalletCommand} instances that operate with the wallet via registered {@link WalletCommandListener} instances.
 * The intention is to provide abstraction of various sources of commands (e.g. standard input, network, file...).
 * </p><p>
 * Regular usage of producer is that client register listeners via {@link #addListener(WalletCommandListener)} and
 * then starts the producer via {@link #start()}.
 * </p><p>
 * It is expected that implementations have its custom logic when producer
 * stops (e.g. special command is read, EOF is reached, socket is closed...etc.) thus there is no method like stop
 * to stop the producer from outside context. However in some cases it may be needed that one producer needs to halt
 * some other producer(s) - typically when running from the main thread. For that purpose, it is advised to pass other
 * producer as regular dependency (ideally as {@link java.io.Closeable} and close such producer manually.
 * </p>
 *
 * @see WalletCommand
 */
public interface WalletCommandsProducer {

    /**
     * Registers given listener so that new {@link WalletCommand} instances will be passed to these listeners when they occur.
     * Each listener is notified only once (even when added multiple times).
     *
     * @param listener listener to be registered
     */
    void addListener(WalletCommandListener listener);

    /**
     * Starts producing commands and notifying registered {@link WalletCommandListener} instances.
     */
    void start();
}

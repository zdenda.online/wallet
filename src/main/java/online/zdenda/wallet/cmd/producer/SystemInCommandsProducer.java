package online.zdenda.wallet.cmd.producer;

import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;
import online.zdenda.wallet.cmd.parser.WalletCommandParser;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 * <p>
 * Represents simple producer that uses system input where user inputs commands via typing into console.
 * Uses {@link WalletCommandParser} for creating instances of {@link WalletCommand}.
 * </p><p>
 * As this producer is usually the main one, it allows to specify also so-called dependent resources
 * (implementations of {@link Closeable}) that are automatically closed when the user wants to quit.
 * </p>
 *
 * @see WalletCommandParser
 */
public class SystemInCommandsProducer implements WalletCommandsProducer {

    private static final String QUIT_COMMAND = "quit";
    private final Collection<WalletCommandListener> listeners = new HashSet<>();
    private final Scanner scanner = new Scanner(System.in);

    private final WalletCommandParser parser;
    private final Collection<Closeable> dependentResources;

    public SystemInCommandsProducer(WalletCommandParser parser, Closeable... dependentResources) {
        this.parser = parser;
        this.dependentResources = List.of(dependentResources);
    }

    @Override
    public void addListener(WalletCommandListener listener) {
        listeners.add(listener);
    }

    @Override
    public void start() {
        String input;
        boolean isQuitCommand;
        do {
            try {
                input = scanner.nextLine().trim();
                isQuitCommand = QUIT_COMMAND.equals(input.toLowerCase());

                if (!isQuitCommand) {
                    Optional<WalletCommand> command = parser.parse(input);
                    if (command.isPresent()) {
                        listeners.forEach(listener -> listener.onCommand(command.get()));
                    } else {
                        System.err.println("'" + input + "' is not a valid command, no action performed but you may proceed with new commands");
                    }
                }
            } catch (NoSuchElementException e) {
                isQuitCommand = true; // user did CTRL+C / CTRL+D (avoids exceptions in console for that situation)
            }

        } while (!isQuitCommand);

        // clean-up
        scanner.close();
        dependentResources.forEach(closeable -> {
            try {
                closeable.close();
            } catch (IOException e) {
                System.err.println("Unable to close related resources, halting JVM");
                System.exit(666); // something evil just happened, world needs to know...
            }
        });
    }
}

package online.zdenda.wallet.cmd.parser;

import online.zdenda.wallet.cmd.ChangeBalanceCommand;
import online.zdenda.wallet.cmd.WalletCommand;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Represents a logic of parsing commands within the system and providing {@link WalletCommand} instances.
 * For future extension (e.g. new implementations), you may introduce an interface from this class.
 */
public class WalletCommandParser {

    /**
     * Parses the input and returns its object representation in form of {@link WalletCommand} object.
     *
     * @param input input to be parsed
     * @return command or empty optional if input is not a valid command
     */
    public Optional<WalletCommand> parse(String input) {
        if (input == null || input.isEmpty()) return Optional.empty();

        // Right now we know only change balance command, future commands can be added here.
        return parseChangeBalanceCommand(input);
    }

    private Optional<WalletCommand> parseChangeBalanceCommand(String input) {
        String[] inputSplit = input.trim().split("\\s+");
        if (inputSplit.length != 2) return Optional.empty();

        Optional<CurrencyUnit> currency = parseCurrency(inputSplit[0].trim());
        if (!currency.isPresent()) return Optional.empty();

        Optional<BigDecimal> amount = parseAmount(inputSplit[1].trim());
        if (!amount.isPresent()) return Optional.empty();

        MonetaryAmount money = Money.of(amount.get(), currency.get());
        return Optional.of(new ChangeBalanceCommand(money));
    }

    private Optional<CurrencyUnit> parseCurrency(String input) {
        try {
            CurrencyUnit currency = Monetary.getCurrency(input);
            return Optional.of(currency);
        } catch (UnknownCurrencyException e) {
            return Optional.empty();
        }
    }

    private Optional<BigDecimal> parseAmount(String input) {
        if (input.isEmpty()) return Optional.empty();

        try {
            return Optional.of(new BigDecimal(input));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}

package online.zdenda.wallet.cmd;

import online.zdenda.wallet.Wallet;

import javax.money.MonetaryAmount;

/**
 * Represents a command that changes the balance of {@link Wallet} by both positive (add) or negative (subtract) amounts.
 *
 * @see WalletCommand
 */
public class ChangeBalanceCommand implements WalletCommand {

    private final MonetaryAmount amount;

    public ChangeBalanceCommand(MonetaryAmount amount) {
        this.amount = amount;
    }

    public MonetaryAmount getAmount() {
        return amount;
    }

    @Override
    public void execute(Wallet wallet) {
        if (amount.isZero()) {
            return;
        }
        if (amount.isPositiveOrZero()) {
            wallet.add(amount);
            return;
        }
        if (amount.isNegative()) {
            wallet.subtract(amount.negate());
        }
    }
}

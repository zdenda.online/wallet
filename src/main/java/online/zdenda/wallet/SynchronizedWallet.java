package online.zdenda.wallet;

import javax.money.MonetaryAmount;
import java.util.Collection;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Decorator of wallet that adds thread safety if {@link Wallet} is used in multi-thread environment.
 * It uses {@link ReentrantReadWriteLock} that allows multiple concurrent {@link #getBalance()} as long as there is
 * not thread writing new values via {@link #add(MonetaryAmount)} or {@link #subtract(MonetaryAmount)}.
 */
public class SynchronizedWallet implements Wallet {

    private final Wallet wallet;
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock(true);

    public SynchronizedWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public Collection<MonetaryAmount> getBalance() {
        rwLock.readLock().lock();
        try {
            return wallet.getBalance();
        } finally {
            rwLock.readLock().unlock();
        }
    }

    @Override
    public void add(MonetaryAmount amount) {
        rwLock.writeLock().lock();
        try {
            wallet.add(amount);
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    @Override
    public void subtract(MonetaryAmount amount) {
        rwLock.writeLock().lock();
        try {
            wallet.subtract(amount);
        } finally {
            rwLock.writeLock().unlock();
        }
    }
}

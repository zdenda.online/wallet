package online.zdenda.wallet;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * {@link Wallet} implementation that stores all data in memory using {@link MonetaryAmount} abstraction for data.
 * Storing in memory effectively means that data are NOT persisted and thus are not preserved per application runs.
 */
public class MemoryWallet implements Wallet {

    private final Map<CurrencyUnit, MonetaryAmount> balance = new HashMap<>();

    @Override
    public Collection<MonetaryAmount> getBalance() {
        return new ArrayList<>(balance.values()); // defensive copy
    }

    @Override
    public void add(MonetaryAmount amount) {
        CurrencyUnit currency = amount.getCurrency();
        MonetaryAmount old = balance.putIfAbsent(currency, amount);
        if (old != null) { // there was no value before
            balance.computeIfPresent(currency, (currencyUnit, monetaryAmount) -> {
                MonetaryAmount newAmount = monetaryAmount.add(amount);
                if (newAmount.isZero()) newAmount = null; // setting to null results in removal of key from map
                return newAmount;
            });
        }
    }

    @Override
    public void subtract(MonetaryAmount amount) {
        CurrencyUnit currency = amount.getCurrency();
        MonetaryAmount old = balance.putIfAbsent(currency, amount.negate());
        if (old != null) { // there was no value before
            balance.computeIfPresent(currency, (currencyUnit, monetaryAmount) -> {
                MonetaryAmount newAmount = monetaryAmount.subtract(amount);
                if (newAmount.isZero()) newAmount = null; // setting to null results in removal of key from map
                return newAmount;
            });
        }
    }
}

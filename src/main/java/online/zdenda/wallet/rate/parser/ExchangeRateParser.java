package online.zdenda.wallet.rate.parser;

import online.zdenda.wallet.rate.ExchangeRate;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Represents logic for parsing exchange rates against main currency.
 */
public class ExchangeRateParser {

    /**
     * Parses the input and returns its object representation in form of {@link ExchangeRate} object.
     *
     * @param input input to be parsed
     * @return exchange rate or empty optional if input is not a valid rate
     */
    public Optional<ExchangeRate> parse(String input) {
        if (input == null || input.isEmpty()) return Optional.empty();

        String[] inputSplit = input.trim().split("\\s+");
        if (inputSplit.length != 2) return Optional.empty();

        Optional<CurrencyUnit> currency = parseCurrency(inputSplit[0].trim());
        if (!currency.isPresent()) return Optional.empty();

        Optional<BigDecimal> multiplicand = parseMultiplicand(inputSplit[1].trim());
        if (!multiplicand.isPresent()) return Optional.empty();

        return Optional.of(new ExchangeRate(currency.get(), multiplicand.get()));
    }

    private Optional<CurrencyUnit> parseCurrency(String input) {
        try {
            CurrencyUnit currency = Monetary.getCurrency(input);
            return Optional.of(currency);
        } catch (UnknownCurrencyException e) {
            return Optional.empty();
        }
    }

    private Optional<BigDecimal> parseMultiplicand(String input) {
        if (input.isEmpty()) return Optional.empty();

        try {
            BigDecimal out = new BigDecimal(input);
            if (out.compareTo(BigDecimal.ZERO) <= 0) {
                return Optional.empty(); // multiplicand must be greater than zero
            }
            return Optional.of(new BigDecimal(input));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}

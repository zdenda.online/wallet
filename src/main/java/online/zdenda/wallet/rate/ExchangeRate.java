package online.zdenda.wallet.rate;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;

/**
 * Represents an exchange rate against main currency.
 */
public class ExchangeRate {

    private final CurrencyUnit currency;
    private final BigDecimal multiplicand;

    public ExchangeRate(CurrencyUnit currency, BigDecimal multiplicand) {
        this.currency = currency;
        this.multiplicand = multiplicand;
    }

    public CurrencyUnit getCurrency() {
        return currency;
    }

    public BigDecimal getMultiplicand() {
        return multiplicand;
    }
}

package online.zdenda.wallet.rate.parser;

import online.zdenda.wallet.rate.ExchangeRate;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ExchangeRateParserTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD");
    private static final CurrencyUnit EUR = Monetary.getCurrency("EUR");

    @Test
    void parsingNullInputReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse(null);

        assertFalse(output.isPresent());
    }

    @Test
    void parsingEmptyInputReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInvalidFormatReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("This is not supported input");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInvalidCurrencyReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("XYZ 10");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingNonNumberMultiplicandReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("USD ABC");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInputWithoutFloatReturnsChangeWalletBalanceCommand() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("USD 10");

        assertExchangeRateOf("10", USD, output);
    }

    @Test
    void parsingInputWithFloatReturnsChangeWalletBalanceCommand() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("EUR 10.27");

        assertExchangeRateOf("10.27", EUR, output);
    }

    @Test
    void parsingNegativeInputReturnsEmptyOptional() {
        ExchangeRateParser parser = new ExchangeRateParser();

        Optional<ExchangeRate> output = parser.parse("USD -10.27");

        assertFalse(output.isPresent());
    }

    private void assertExchangeRateOf(String multiplicand, CurrencyUnit currency, Optional<ExchangeRate> rate) {
        assertTrue(rate.isPresent());
        assertEquals(currency, rate.get().getCurrency());
        assertEquals(new BigDecimal(multiplicand), rate.get().getMultiplicand());
    }
}

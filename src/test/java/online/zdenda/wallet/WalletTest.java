package online.zdenda.wallet;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests all {@link Wallet} implementations that they satisfy the contract of {@link Wallet} interface.
 */
class WalletTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD");
    private static final CurrencyUnit EUR = Monetary.getCurrency("EUR");
    private static final CurrencyUnit CZK = Monetary.getCurrency("CZK");

    private void testWallet(Consumer<Wallet> walletFunction) {
        List<Wallet> implementations = List.of(
                new MemoryWallet(),
                new SynchronizedWallet(new MemoryWallet())
        ); // add new implementations here
        implementations.forEach(walletFunction);
    }

    @Test
    void newWalletHasEmptyBalance() {
        testWallet(wallet -> {
            assertNotNull(wallet.getBalance());
            assertEquals(0, wallet.getBalance().size());

            assertNoBalanceFor(USD, wallet);
            assertNoBalanceFor(EUR, wallet);
            assertNoBalanceFor(CZK, wallet);
        });
    }

    @Test
    void addSingleValueResultsInCorrectAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10, USD));
            assertBalanceOf(10, USD, wallet);

            // check to be sure it works even when value of difference currency is added
            assertNoBalanceFor(EUR, wallet);
            assertNoBalanceFor(CZK, wallet);
        });
    }

    @Test
    void addSingleValueOfMultipleCurrenciesResultsInCorrectAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10.1, USD));
            wallet.add(Money.of(27, EUR));
            wallet.add(Money.of(100, CZK));

            assertBalanceOf(10.1, USD, wallet);
            assertBalanceOf(27, EUR, wallet);
            assertBalanceOf(100, CZK, wallet);
        });
    }

    @Test
    void addMultipleValuesOfSingleCurrencyResultsInCorrectAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10, USD));
            wallet.add(Money.of(5, USD));
            wallet.add(Money.of(12.333, USD));

            assertBalanceOf(27.333, USD, wallet);
        });
    }

    @Test
    void addMultipleValuesOfMultipleCurrenciesResultsInCorrectAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10, USD));
            wallet.add(Money.of(5, USD));
            wallet.add(Money.of(3, EUR));
            wallet.add(Money.of(3.14, EUR));
            wallet.add(Money.of(7, CZK));
            wallet.add(Money.of(2.5555, CZK));

            assertBalanceOf(15, USD, wallet);
            assertBalanceOf(6.14, EUR, wallet);
            assertBalanceOf(9.5555, CZK, wallet);
        });
    }

    @Test
    void addAndSubtractMultipleValuesResultsInCorrectPositiveAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10, USD));
            wallet.add(Money.of(15, USD));
            wallet.subtract(Money.of(20, USD));

            assertBalanceOf(5, USD, wallet);
        });
    }

    @Test
    void addAndSubtractMultipleValuesResultsInCorrectNegativeAmountsInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10, USD));
            wallet.add(Money.of(15, USD));
            wallet.subtract(Money.of(50.7, USD));

            assertBalanceOf(-25.7, USD, wallet);
        });
    }

    @Test
    void addAndSubtractToZeroResultsInNoCurrencyInBalance() {
        testWallet(wallet -> {
            wallet.add(Money.of(10.7, USD));
            wallet.add(Money.of(3.3, USD));
            wallet.subtract(Money.of(10.3, USD));
            wallet.subtract(Money.of(3.7, USD));

            assertNoBalanceFor(USD, wallet);
        });
    }

    @Test
    void subtractAndAddToZeroResultsInNoCurrencyInBalance() {
        testWallet(wallet -> {
            wallet.subtract(Money.of(10.3, USD));
            wallet.subtract(Money.of(3.7, USD));
            wallet.add(Money.of(10.7, USD));
            wallet.add(Money.of(3.3, USD));

            assertNoBalanceFor(USD, wallet);
        });
    }

    private void assertBalanceOf(Number amount, CurrencyUnit currency, Wallet wallet) {
        Collection<MonetaryAmount> balance = wallet.getBalance();
        MonetaryAmount presentAmount = balance.stream()
                .filter(monetaryAmount -> monetaryAmount.getCurrency() == currency)
                .findFirst()
                .orElse(null);

        if (presentAmount == null) fail("Expected entry for currency " + currency + " but not present in wallet");
        MonetaryAmount expected = Money.of(amount, currency);
        assertTrue(presentAmount.isEqualTo(expected));
    }

    private void assertNoBalanceFor(CurrencyUnit currency, Wallet wallet) {
        Collection<MonetaryAmount> balance = wallet.getBalance();
        balance.stream()
                .filter(monetaryAmount -> monetaryAmount.getCurrency() == currency)
                .findFirst()
                .ifPresent(presentAmount -> fail("Expected no entry for currency " + currency + " but is present in wallet"));
    }
}

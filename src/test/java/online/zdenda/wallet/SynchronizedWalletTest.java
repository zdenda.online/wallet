package online.zdenda.wallet;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class SynchronizedWalletTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD");

    /**
     * Tests multiple concurrent "writes" (add operations) into {@link SynchronizedWallet}.
     * Note that it "tries its best" to make all calls concurrent. If you want to try how effective that is, you may
     * use memoryWallet in threads submitted to executor and in the last call to balance - it should fail if plain
     * wallet is used instead of synchronized one.
     */
    @Test
    void concurrentAddsResultInCorrectFinalBalance() throws InterruptedException {
        Wallet wallet = aSynchronizedWallet();
        int runs = 10000;
        int threadPoolSize = 1000;
        long sleepBeforeAdd = 10;

        ExecutorService execService = Executors.newFixedThreadPool(threadPoolSize);
        for (int i = 0; i < runs; i++) {
            execService.submit(() -> {
                try {
                    Thread.sleep(sleepBeforeAdd); // wait a bit so there is higher chance that threads "meet"
                    wallet.add(Money.of(1, USD));
                } catch (InterruptedException e) {
                    // never happens in this case
                }
            });
        }
        long approxDuration = (runs / threadPoolSize) * sleepBeforeAdd;
        execService.awaitTermination(approxDuration + 500, TimeUnit.MILLISECONDS); // +500ms to be sure

        MonetaryAmount balance = wallet.getBalance().stream().findFirst().orElse(null);
        assertNotNull(balance);
        assertEquals(Money.of(runs, USD), balance);
    }

    /**
     * Does the same as above test but does both add and subtract concurrently and checks that balance is zero
     * at the end.
     */
    @Test
    void concurrentAddAndSubtractResultInZeroFinalBalance() throws InterruptedException {
        Wallet wallet = aSynchronizedWallet();
        int runs = 10000;
        int threadPoolSize = 1000;
        long sleepBeforeAdd = 10;

        ExecutorService execService = Executors.newFixedThreadPool(threadPoolSize);
        for (int i = 0; i < runs; i++) {
            execService.submit(() -> {
                try {
                    Thread.sleep(sleepBeforeAdd); // wait a bit so there is higher chance that threads "meet"
                    wallet.add(Money.of(1, USD));
                } catch (InterruptedException ignored) {
                }
            });
            execService.submit(() -> {
                try {
                    Thread.sleep(sleepBeforeAdd); // wait a bit so there is higher chance that threads "meet"
                    wallet.subtract(Money.of(1, USD));
                } catch (InterruptedException ignored) {
                }
            });
        }
        long approxDuration = (2 * runs / threadPoolSize) * sleepBeforeAdd;
        execService.awaitTermination(approxDuration + 1000, TimeUnit.MILLISECONDS); // +500ms to be sure

        Collection<MonetaryAmount> balance = wallet.getBalance();
        assertTrue(balance.isEmpty()); // zero means there is no value
    }

    /**
     * Does the same as above but also adds some random balance reads.
     */
    @Test
    void concurrentAddAndSubtractWithRandomBalanceReadsResultInZeroFinalBalance() throws InterruptedException {
        Wallet wallet = aSynchronizedWallet();
        int runs = 10000;
        int threadPoolSize = 1000;
        long sleepBeforeAdd = 10;

        ExecutorService execService = Executors.newFixedThreadPool(threadPoolSize);
        for (int i = 0; i < runs; i++) {
            execService.submit(() -> {
                try {
                    Thread.sleep(sleepBeforeAdd); // wait a bit so there is higher chance that threads "meet"
                    wallet.add(Money.of(1, USD));
                } catch (InterruptedException ignored) {
                }
            });
            if (Math.random() > 0.7) {
                execService.submit(wallet::getBalance);
            }
            execService.submit(() -> {
                try {
                    Thread.sleep(sleepBeforeAdd); // wait a bit so there is higher chance that threads "meet"
                    wallet.subtract(Money.of(1, USD));
                } catch (InterruptedException ignored) {
                }
            });
        }
        long approxDuration = (2 * runs / threadPoolSize) * sleepBeforeAdd;
        execService.awaitTermination(approxDuration + 1000, TimeUnit.MILLISECONDS); // +500ms to be sure

        Collection<MonetaryAmount> balance = wallet.getBalance();
        assertTrue(balance.isEmpty()); // zero means there is no value
    }

    private Wallet aSynchronizedWallet() {
        Wallet memoryWallet = new MemoryWallet();  // cannot use mock as it is thread safe and I would test nothing
        return new SynchronizedWallet(memoryWallet);
    }
}

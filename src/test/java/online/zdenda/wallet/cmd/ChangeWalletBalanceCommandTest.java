package online.zdenda.wallet.cmd;

import online.zdenda.wallet.Wallet;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChangeWalletBalanceCommandTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD");

    @Test
    void zeroAmountDoesNotInvokeAnyWalletMethod() {
        Wallet wallet = aWallet();
        MonetaryAmount amount = Money.of(0, USD);
        WalletCommand cmd = new ChangeBalanceCommand(amount);

        cmd.execute(wallet);

        verifyNoMoreInteractions(wallet);
    }

    @Test
    void positiveAmountInvokesWalletAdd() {
        Wallet wallet = aWallet();
        MonetaryAmount amount = Money.of(10, USD);
        WalletCommand cmd = new ChangeBalanceCommand(amount);

        cmd.execute(wallet);

        verify(wallet).add(amount);
        verifyNoMoreInteractions(wallet);
    }

    @Test
    void negativeAmountInvokesWalletSubtract() {
        Wallet wallet = aWallet();
        MonetaryAmount amount = Money.of(-10.3, USD);
        WalletCommand cmd = new ChangeBalanceCommand(amount);

        cmd.execute(wallet);

        verify(wallet).subtract(amount.negate()); // !! this must be negated value when calling subtract
        verifyNoMoreInteractions(wallet);
    }

    private Wallet aWallet() {
        return mock(Wallet.class);
    }
}

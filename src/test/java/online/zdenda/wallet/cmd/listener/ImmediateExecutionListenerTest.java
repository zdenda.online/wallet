package online.zdenda.wallet.cmd.listener;

import online.zdenda.wallet.Wallet;
import online.zdenda.wallet.cmd.WalletCommand;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImmediateExecutionListenerTest {

    @Test
    void executesImmediatelyOnlyOnce() {
        Wallet wallet = aWallet();
        WalletCommand command = aCommand();
        WalletCommandListener listener = new ImmediateExecutionListener(wallet);

        listener.onCommand(command);

        verify(command).execute(wallet); // exactly once
        verifyNoMoreInteractions(command);
    }

    private WalletCommand aCommand() {
        return mock(WalletCommand.class);
    }

    private Wallet aWallet() {
        return mock(Wallet.class);
    }

}

package online.zdenda.wallet.cmd.producer;

import online.zdenda.wallet.cmd.WalletCommand;
import online.zdenda.wallet.cmd.listener.WalletCommandListener;
import online.zdenda.wallet.cmd.parser.WalletCommandParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileCommandsProducerTest {

    private static final String FILE_LINE = "x";
    private ByteArrayOutputStream customErr;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    void setUpStreams() {
        customErr = new ByteArrayOutputStream();
        System.setErr(new PrintStream(customErr));
    }

    @AfterEach
    void restoreStreams() {
        System.setOut(originalErr);
    }

    @Test
    void nonExistentFilePrintsErrorAndDoesNotInteractWithListeners() {
        WalletCommandListener listener = aListener();
        WalletCommandsProducer producer = new FileCommandsProducer(nonExistentFile(), aParser());
        producer.addListener(listener);

        producer.start();

        assertTrue(customErr.toString().contains("does not exist. Nothing will be added"));
        verifyNoMoreInteractions(listener);
    }

    @Test
    void invalidCommandsInFileDoesNotNotifyListenerAndPrintError() {
        WalletCommandListener listener = aListener();
        File file = newTempFileWithThreeLines();
        WalletCommandParser parser = aParser();
        WalletCommandsProducer producer = new FileCommandsProducer(file, parser);
        producer.addListener(listener);

        when(parser.parse(FILE_LINE)).thenReturn(Optional.empty());

        producer.start();

        assertTrue(customErr.toString().contains("is not a valid command, no action performed"));
        verifyNoMoreInteractions(listener);
        file.deleteOnExit();
    }

    @Test
    void validCommandsInFileNotifyListener() {
        WalletCommandListener listener = aListener();
        File file = newTempFileWithThreeLines();
        WalletCommandParser parser = aParser();
        WalletCommandsProducer producer = new FileCommandsProducer(file, parser);
        producer.addListener(listener);

        WalletCommand cmd = aCommand();
        when(parser.parse(FILE_LINE)).thenReturn(Optional.of(cmd));

        producer.start();

        verify(listener, times(3)).onCommand(cmd);
        file.deleteOnExit();
    }

    private WalletCommand aCommand() {
        return mock(WalletCommand.class);
    }

    private File nonExistentFile() {
        return new File("thisFileDoesNotExistForSure");
    }

    private File newTempFileWithThreeLines() {
        try {
            File file = File.createTempFile("wallet", "test");
            Files.write(file.toPath(), (FILE_LINE + "\n" + FILE_LINE + "\n" + FILE_LINE).getBytes(StandardCharsets.UTF_8));
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private WalletCommandParser aParser() {
        return mock(WalletCommandParser.class);
    }

    private WalletCommandListener aListener() {
        return mock(WalletCommandListener.class);
    }
}

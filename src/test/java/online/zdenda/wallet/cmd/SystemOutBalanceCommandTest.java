package online.zdenda.wallet.cmd;

import online.zdenda.wallet.Wallet;
import online.zdenda.wallet.format.WalletFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SystemOutBalanceCommandTest {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private ByteArrayOutputStream customOut;
    private final PrintStream originalOut = System.out;

    @BeforeEach
    void setUpStreams() {
        customOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(customOut));
    }

    @AfterEach
    void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void printsWhatFormatterProduces() {
        Wallet wallet = aWallet();
        WalletFormatter formatter = aWalletFormatter();
        WalletCommand cmd = new SystemOutBalanceCommand(formatter);
        when(formatter.format(wallet)).thenReturn("A\nB\nC");

        cmd.execute(wallet);

        assertEquals("A\nB\nC" + LINE_SEPARATOR, customOut.toString()); // printLN (must expect line separator)
    }

    private Wallet aWallet() {
        return mock(Wallet.class);
    }

    private WalletFormatter aWalletFormatter() {
        return mock(WalletFormatter.class);
    }
}

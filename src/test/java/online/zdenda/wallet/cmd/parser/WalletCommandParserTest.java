package online.zdenda.wallet.cmd.parser;

import online.zdenda.wallet.cmd.ChangeBalanceCommand;
import online.zdenda.wallet.cmd.WalletCommand;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class WalletCommandParserTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD");
    private static final CurrencyUnit EUR = Monetary.getCurrency("EUR");

    @Test
    void parsingNullInputReturnsEmptyOptional() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse(null);

        assertFalse(output.isPresent());
    }

    @Test
    void parsingEmptyInputReturnsEmptyOptional() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInvalidFormatReturnsEmptyOptional() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("This is not supported input");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInvalidCurrencyReturnsEmptyOptional() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("XYZ 10");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingNonNumberAmountReturnsEmptyOptional() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("USD ABC");

        assertFalse(output.isPresent());
    }

    @Test
    void parsingInputWithoutFloatReturnsChangeWalletBalanceCommand() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("EUR 10");

        assertChangeBalanceCommand("10", EUR, output);
    }

    @Test
    void parsingInputWithFloatReturnsChangeWalletBalanceCommand() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("USD 10.27");

        assertChangeBalanceCommand("10.27", USD, output);
    }

    @Test
    void parsingNegativeInputWithFloatReturnsChangeWalletBalanceCommand() {
        WalletCommandParser parser = new WalletCommandParser();

        Optional<WalletCommand> output = parser.parse("USD -10.27");

        assertChangeBalanceCommand("-10.27", USD, output);
    }

    private void assertChangeBalanceCommand(String amount, CurrencyUnit currency, Optional<WalletCommand> command) {
        assertTrue(command.isPresent());
        assertTrue(command.get() instanceof ChangeBalanceCommand);
        MonetaryAmount monetaryAmount = ((ChangeBalanceCommand) command.get()).getAmount();
        assertEquals(currency, monetaryAmount.getCurrency());
        assertEquals(currency.toString() + " " + amount, monetaryAmount.toString());
    }
}

package online.zdenda.wallet.format;

import online.zdenda.wallet.rate.ExchangeRate;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AmountWithConversionFormatterTest {

    private static final CurrencyUnit USD = Monetary.getCurrency("USD"); // main currency
    private static final CurrencyUnit EUR = Monetary.getCurrency("EUR");
    private static final CurrencyUnit CZK = Monetary.getCurrency("CZK");

    @Test
    void zeroAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = aFormatter(aRate(EUR, "1.1"));
        MonetaryAmount amount = Money.of(0, EUR);

        String out = formatter.format(amount);

        assertEquals("EUR:0 (USD:0)", out);
    }

    @Test
    void positiveAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = aFormatter(aRate(EUR, "1.1"));
        MonetaryAmount amount = Money.of(10, EUR);

        String out = formatter.format(amount);

        assertEquals("EUR:10 (USD:11)", out);
    }

    @Test
    void negativeAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = aFormatter(aRate(CZK, "0.04"));
        MonetaryAmount amount = Money.of(-250, CZK);

        String out = formatter.format(amount);

        assertEquals("CZK:-250 (USD:-10)", out);
    }

    @Test
    void amountInCurrencyWithoutRateDoesNotAddAnything() {
        MonetaryAmountFormatter formatter = aFormatter(aRate(EUR, "25"));
        MonetaryAmount amount = Money.of(-10, CZK);

        String out = formatter.format(amount);

        assertEquals("CZK:-10", out);
    }

    private MonetaryAmountFormatter aFormatter(ExchangeRate... exchangeRates) {
        MonetaryAmountFormatter decorated = new SemicolonFormatter();
        return new AmountWithConversionFormatter(decorated, USD, Set.of(exchangeRates));
    }


    private ExchangeRate aRate(CurrencyUnit currency, String multiplicand) {
        return new ExchangeRate(currency, new BigDecimal(multiplicand));
    }

    /**
     * Using custom formatter for testing to verify that additional conversion is formatter via decorated formatter.
     */
    private static class SemicolonFormatter implements MonetaryAmountFormatter {

        @Override
        public String format(MonetaryAmount amount) {
            String formattedNumber = NumberFormat.getNumberInstance().format(amount.getNumber());
            return amount.getCurrency() + ":" + formattedNumber;
        }
    }
}

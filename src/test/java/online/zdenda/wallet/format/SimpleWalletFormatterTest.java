package online.zdenda.wallet.format;

import online.zdenda.wallet.Wallet;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.money.MonetaryAmount;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SimpleWalletFormatterTest {

    @Test
    void emptyWalletFormatsToEmptyString() {
        Wallet wallet = aWallet();
        WalletFormatter formatter = new SimpleWalletFormatter(new SemicolonFormatter());

        String out = formatter.format(wallet);

        assertEquals(balanceWith(""), out);
    }

    @Test
    void walletWithSingleCurrencyFormatsCorrectly() {
        Wallet wallet = aWallet(Money.of(10, "USD"));
        WalletFormatter formatter = new SimpleWalletFormatter(new SemicolonFormatter());

        String out = formatter.format(wallet);

        assertEquals(balanceWith("USD:10"), out);
    }

    @Test
    void walletWithMultipleCurrenciesFormatsCorrectly() {
        Wallet wallet = aWallet(
                Money.of(10, "USD"),
                Money.of(15.3, "EUR"),
                Money.of(-27.27, "CZK"));
        WalletFormatter formatter = new SimpleWalletFormatter(new SemicolonFormatter());

        String out = formatter.format(wallet);

        assertEquals(balanceWith("USD:10\nEUR:15.3\nCZK:-27.27"), out);
    }

    private Wallet aWallet() {
        return mock(Wallet.class);
    }

    private Wallet aWallet(MonetaryAmount... amounts) {
        Wallet wallet = aWallet();
        when(wallet.getBalance()).thenReturn(List.of(amounts));
        return wallet;
    }

    private String balanceWith(String value) {
        return SimpleWalletFormatter.PREFIX + value + SimpleWalletFormatter.SUFFIX;
    }

    /**
     * Using custom formatter for testing to verify that additional conversion is formatter via decorated formatter.
     */
    private static class SemicolonFormatter implements MonetaryAmountFormatter {

        @Override
        public String format(MonetaryAmount amount) {
            return amount.getCurrency() + ":" + amount.getNumber();
        }
    }
}

package online.zdenda.wallet.format;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import javax.money.MonetaryAmount;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleAmountFormatterTest {

    @Test
    void zeroAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = new SimpleAmountFormatter();
        MonetaryAmount amount = Money.of(0, "USD");

        String out = formatter.format(amount);

        assertEquals("USD 0", out);
    }

    @Test
    void regularAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = new SimpleAmountFormatter();
        MonetaryAmount amount = Money.of(10.27, "USD");

        String out = formatter.format(amount);

        assertEquals("USD 10.27", out);
    }

    @Test
    void negativeAmountIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = new SimpleAmountFormatter();
        MonetaryAmount amount = Money.of(-13.3, "USD");

        String out = formatter.format(amount);

        assertEquals("USD -13.3", out);
    }

    @Test
    void floatingTenIsFormattedCorrectly() {
        MonetaryAmountFormatter formatter = new SimpleAmountFormatter();
        MonetaryAmount amount = Money.of(-10.0, "USD");

        String out = formatter.format(amount);

        assertEquals("USD -10", out); // BigDecimal by default prints -1E+1
    }
}
